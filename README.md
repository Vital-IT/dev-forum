# Vital-IT/SIB developer forum
Held biweekly on Tuesdays in Génopode at noon (usually in room 2020 or 2019), the goal is to gather folks involved in scientific software development.
Talks encompass a large variety of topics.

Come to attend! Come to present!

## Program

| When? | Where? | Who? | What?|slides|
|-------|--------|------|------|------|
|12/16/2014|2019|Martial Sanskar|Interactive charts and web app using RCharts and Shiny|[slides](slides/20141216_web_r.html)
|2/3/2015|2020|Nicolas Budin|Tools used for development and testing of MegaClust Javascript backend and front end|
|2/17/2015|2020|Manohar Jonnalagedda|Abstraction without Regret for high-level, high performance programming|
|3/10/2015|2020|Alex Masselot|Scala: 6 silver bullets|[slides](slides/20150309_dev_forum_scala)
|3/24/2015|2019|Ivan Topolsky|Git at SIB|[slides](slides/20150324_git@vital-it.pdf)
|4/7/2015|2020|Olivier Martin|What's new in Java 8?|
|4/21/2015|2020|Roman Mylonas|Jenkins: continuous building and integration|[slides](slides/20150419_jenkins.pdf)
|5/5/2015|2020|Fred Schutz|R & knitr|
|5/19/2015|2019|Robin Liechti|Cytoscape.js|[slides](https://gitlab.isb-sib.ch/rliechti/cytoscape_js_pres)
|6/2/2015|2019|Alex Masselot|MongoDB|[slides](https://gitlab.isb-sib.ch/amasselo/mongodb_devforum_20150601)
|6/16/2015|2020|Thierry Schuepabch|Raspberry PI|
|6/30/2015|2020|Dmitry Kuznetsov|An introduction RDF/SparQL|
